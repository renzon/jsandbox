import json
from google.appengine.ext import ndb
from model import Pergunta,Resposta

def salvar(resp,pergunta_id,texto):
    pergunta_key=ndb.Key(Pergunta,long(pergunta_id))
    resposta=Resposta(texto=texto,pergunta=pergunta_key)
    resposta.put()
    d=resposta.to_dict()
    resp.write(json.dumps(d))

def listar(resp,pergunta_id):
    pergunta_key=ndb.Key(Pergunta,long(pergunta_id))
    respostas=Resposta.ordenadas_criacao(pergunta_key).fetch(10)
    respostas=[r.to_dict() for r in respostas]
    resp.write(json.dumps(respostas))


