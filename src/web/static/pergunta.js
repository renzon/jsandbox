define(["resposta", "module", "mustache","text!tmpl/pergunta.html", "jquery"],
function(resposta, module, mustache, perguntaTemplate, $){
    var perguntaListarUrl = module.config().perguntaListarUrl;
    var perguntaUrl = module.config().perguntaUrl;
    var perguntaBotaoSelector = module.config().perguntaBotaoSelector;
    var perguntaContainerSelector = module.config().perguntaContainerSelector;

    function renderizarPergunta(pergunta){
        var rendered = mustache.render(perguntaTemplate,pergunta);
        var aux = $("<div />");
        aux.html(rendered);
        var toPrepend = aux.children();
        resposta(aux, pergunta.id);
        toPrepend.hide();
        $(perguntaContainerSelector).prepend(toPrepend);
        toPrepend.slideDown();

    }

    function mostrarPerguntas(perguntas){
        $.each(perguntas, function (i, pergunta) {
            renderizarPergunta(pergunta);
        });
    }

    $(document).ready(function(){
        $.getJSON(perguntaListarUrl, mostrarPerguntas);

        function salvarPergunta() {
            var $textArea = $("#pergunta_txt");
            var dadosPost = {'texto':$textArea.val()};
            $.post(perguntaUrl, dadosPost, renderizarPergunta,"json");
            $textArea.val("");
        }

        $(perguntaBotaoSelector).click(salvarPergunta);
    });


});
