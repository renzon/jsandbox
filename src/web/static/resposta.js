define(["mustache", "text!tmpl/resposta.html", "module", "jquery"],
function(mustache, respostaTmpl,module, $){
    var respostaUrlBase = module.config().respostaUrl;
    var respostaListarUrlBase = module.config().respostaListarUrl;
    var inputSelector = "textarea";
    var respostaContainerSelector = "div.resposta-container";

    return function ($perguntaContainer,perguntaId) {
        var $textArea = $perguntaContainer.find(inputSelector);
        var $respostasContainer = $perguntaContainer.find(respostaContainerSelector);
        var respostaUrl = respostaUrlBase + perguntaId;
        var respostaListarUrl = respostaListarUrlBase + perguntaId;

        function renderizarResposta(resposta) {
            var rendered=mustache.render(respostaTmpl,resposta);
            var aux = $("<div />");
            aux.html(rendered);
            var toAppend = aux.children();
            toAppend.hide();
            $respostasContainer.append(toAppend);
            toAppend.slideDown();
        }

        function salvarReposta() {
            var dadosPost = {'texto' : $textArea.val()};
            $.post(respostaUrl, dadosPost, renderizarResposta, "json");
            $textArea.val("");
        }

        $perguntaContainer.find("button").click(salvarReposta);

        $.getJSON(respostaListarUrl, function(respostas){
            $.each(respostas, function (i, resposta){
                renderizarResposta(resposta);
            });
        });
    }
});
