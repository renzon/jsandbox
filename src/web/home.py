from model import Pergunta, Resposta
from web import pergunta, resposta
from zen import router

def index(write_template):
    valores_do_template={"pergunta_listar_url":router.to_path(pergunta.listar),
                         "pergunta_url":router.to_path(pergunta.salvar),
                         "resposta_listar_url":router.to_path(resposta.listar),
                         "resposta_url":router.to_path(resposta.salvar)}
    write_template("templates/home.html",valores_do_template)
