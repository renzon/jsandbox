import json
from model import Pergunta

def salvar(resp,texto):
    pergunta=Pergunta(texto=texto)
    pergunta.put()
    resp.write(json.dumps(pergunta.to_dict()))

def listar(resp):
    perguntas=Pergunta.ordenadas_criacao().fetch(10)
    perguntas=[p.to_dict() for p in perguntas]
    resp.write(json.dumps(perguntas))
