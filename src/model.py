from google.appengine.ext import ndb
from zen.dataprocess import filters

class Pergunta(ndb.Model):
    texto=ndb.TextProperty(required=True)
    criacao=ndb.DateTimeProperty(auto_now_add=True)

    @classmethod
    def ordenadas_criacao(cls):
        return cls.query().order(-Pergunta.criacao)

    def to_dict(self):
        return {"id":self.key.id(),"texto":self.texto,
                "criacao":filters.brdate(self.criacao)}


class Resposta(ndb.Model):
    texto=ndb.TextProperty(required=True)
    criacao=ndb.DateTimeProperty(auto_now_add=True)
    pergunta=ndb.KeyProperty(Pergunta,required=True)

    @classmethod
    def ordenadas_criacao(cls, pergunta_key):
        return cls.query(Resposta.pergunta==pergunta_key).order(Resposta.criacao)

    def to_dict(self):
        return {"id":self.key.id(),"texto":self.texto,
                "criacao":filters.brdate(self.criacao)}

